package com.trulycanadian.ExpenseRecorder.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.trulycanadian.ExpenseRecorder.R;
import com.trulycanadian.ExpenseRecorder.db.CategoryExpenseDB;
import com.trulycanadian.ExpenseRecorder.db.CategoryIncomeDB;

public class AddCategoryActivity extends Activity {
	EditText itemEditText;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.addcategory);
		setupButton();
		populateSpinner();
	}

	void setupButton() {
		Button saveButton;
		saveButton = (Button) findViewById(R.id.saveButton);
		saveButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				saveItem();
			}
		});

	}

	void populateSpinner() {
		Spinner m_myDynamicSpinner;
		ArrayAdapter<String> m_adapterForSpinner;
		m_myDynamicSpinner = (Spinner) findViewById(R.id.spinner);
		m_adapterForSpinner = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item);
		m_adapterForSpinner
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		m_myDynamicSpinner.setAdapter(m_adapterForSpinner);
		m_adapterForSpinner.add("Income Category");
		m_adapterForSpinner.add("Expense Category");
	}

	void saveItem() {
		boolean success = true;
		Spinner m_myDynamicSpinner;
		itemEditText = (EditText) findViewById(R.id.categoryName);
		String categoryName = itemEditText.getText().toString();
		if (categoryName.equals(""))
			callError("Sorry entry can not be blank");
		else {
			m_myDynamicSpinner = (Spinner) findViewById(R.id.spinner);
			if (m_myDynamicSpinner.getSelectedItem().toString().equals(
					"Income Category"))
				success = insertIncomeCategory();
			else
				success = insertExpenseCategory();
			if (success)
				finish();
		}
	}

	boolean insertIncomeCategory() {
		boolean success = true;
		CategoryIncomeDB categoryIncomeDB = new CategoryIncomeDB(this);
		itemEditText = (EditText) findViewById(R.id.categoryName);
		String categoryName = itemEditText.getText().toString();
		if (!(categoryIncomeDB.checkHasCategoryExists(categoryName)))
			categoryIncomeDB.addCategory(categoryName);
		else {
			callError("Category Already Exists");
			success = false;
		}
		return success;
	}

	boolean insertExpenseCategory() {
		boolean success = true;
		CategoryExpenseDB categoryExpenseDB = new CategoryExpenseDB(this);
		itemEditText = (EditText) findViewById(R.id.categoryName);
		String categoryName = itemEditText.getText().toString();
		if (!(categoryExpenseDB.checkHasCategoryExists(categoryName)))
			categoryExpenseDB.addCategory(categoryName);
		else {
			success = false;
			callError("Category Already Exists");
		}
		return success;
	}

	void callError(String entry) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
			}
		});
		builder.setMessage(entry);
		AlertDialog alert = builder.create();
		alert.show();
	}
}
