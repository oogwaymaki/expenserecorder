package com.trulycanadian.ExpenseRecorder.Activity;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.trulycanadian.ExpenseRecorder.Category;
import com.trulycanadian.ExpenseRecorder.R;
import com.trulycanadian.ExpenseRecorder.ListAdapter.CustomSpinnerAdapter;
import com.trulycanadian.ExpenseRecorder.db.CategoryExpenseDB;
import com.trulycanadian.ExpenseRecorder.db.CategoryIncomeDB;
import com.trulycanadian.ExpenseRecorder.db.ItemExpenseDB;
import com.trulycanadian.ExpenseRecorder.db.ItemIncomeDB;

public class DeleteCategoryActivity extends Activity {
	EditText itemEditText;
	Spinner spinnerType;
	Spinner spinnerCategory;
	HashMap<String, Long> spinnerHash = new HashMap<String, Long>();
	private Spinner.OnItemSelectedListener categoryListener =

	new Spinner.OnItemSelectedListener() {

		public void onItemSelected(AdapterView parent, View v, int position,
				long id) {

			if (spinnerType.getSelectedItem().toString().equals(
					"Income Category")) {
				System.out.println("Got here");
				CategoryIncomeDB categoryLoader = new CategoryIncomeDB(
						DeleteCategoryActivity.this);
				CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(
						DeleteCategoryActivity.this,
						android.R.layout.simple_spinner_item);
				ArrayList<Category> categories = new ArrayList<Category>();
				categories = categoryLoader.getAllCategory();
				for (Category category : categories) {
					customSpinnerAdapter.addItem(category.getName(), category
							.getCatid());
				}
				customSpinnerAdapter.notifyDataSetChanged();
				spinnerCategory.setAdapter(customSpinnerAdapter);
			}

			if (spinnerType.getSelectedItem().toString().equals(
					"Expense Category")) {
				CategoryExpenseDB categoryLoader = new CategoryExpenseDB(
						DeleteCategoryActivity.this);
				CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(
						DeleteCategoryActivity.this,
						android.R.layout.simple_spinner_item);
				ArrayList<Category> categories = new ArrayList<Category>();
				categories = categoryLoader.getAllCategory();
				for (Category category : categories) {
					customSpinnerAdapter.addItem(category.getName(), category
							.getCatid());
				}
				customSpinnerAdapter.notifyDataSetChanged();
				spinnerCategory.setAdapter(customSpinnerAdapter);
			}
		}

		public void onNothingSelected(AdapterView parent) {
		}

	};

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.deletecategory);
		setupButton();
		populateSpinner();
	}

	void setupButton() {
		Button deleteButton;
		deleteButton = (Button) findViewById(R.id.saveButton);
		deleteButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				String date = "";
				boolean success = true;
				spinnerCategory = (Spinner) findViewById(R.id.spinnercategory);
				System.out.println("Category = " + spinnerCategory.getSelectedItem().toString());
				if (spinnerType.getSelectedItem().toString().equals(
						"Income Category")) {
					ItemIncomeDB itemLoader = new ItemIncomeDB(
							DeleteCategoryActivity.this);
					date = itemLoader.checkCategoryHasItem(spinnerCategory
							.getSelectedItemId());
					System.out.println("Spinner Category" + spinnerCategory.getSelectedItemId());
					if (date == null) {
						System.out.println("Inside Date" + spinnerCategory.getSelectedItemId());
						CategoryIncomeDB categoryIncomeDeleter = new CategoryIncomeDB(
								DeleteCategoryActivity.this);
						categoryIncomeDeleter.deleteRecord(spinnerCategory
								.getSelectedItemId());
					} else {
						success = false;
						callError("Sorry there are items starting from the date "
								+ date + "\n Please Remove or change the items category before deleting");
					}
				}
				if (spinnerType.getSelectedItem().toString().equals(
						"Expense Category")) {
					ItemExpenseDB itemLoader = new ItemExpenseDB(
							DeleteCategoryActivity.this);
					date = itemLoader.checkCategoryHasItem(spinnerCategory
							.getSelectedItemId());
					if (date == null) {
						CategoryExpenseDB categoryExpenseDeleter = new CategoryExpenseDB(
								DeleteCategoryActivity.this);
						categoryExpenseDeleter.deleteRecord(spinnerCategory
								.getSelectedItemId());
					} else {
						success = false;
						callError("Sorry there are items starting from the date "
								+ date + "\n Please Remove or change the items category before deleting");
					}
				}
				if (success == true)
					finish();

			}
		});

	}
	void callError(String entry)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
							int item) {
					}
				});
		builder
				.setMessage(entry);
		AlertDialog alert = builder.create();
		alert.show();
	}
	void populateSpinner() {

		ArrayAdapter<String> m_adapterForSpinner;
		spinnerType = (Spinner) findViewById(R.id.spinnertype);
		spinnerCategory = (Spinner) findViewById(R.id.spinnercategory);
		m_adapterForSpinner = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item);
		m_adapterForSpinner
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerType.setAdapter(m_adapterForSpinner);
		m_adapterForSpinner.add("Income Category");
		m_adapterForSpinner.add("Expense Category");
		spinnerType.setOnItemSelectedListener(categoryListener);
		spinnerType.setSelection(0);
	}
}
