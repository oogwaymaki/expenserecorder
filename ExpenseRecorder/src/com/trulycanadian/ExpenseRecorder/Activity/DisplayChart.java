package com.trulycanadian.ExpenseRecorder.Activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import org.achartengine.ChartFactory;
import org.achartengine.chart.PieChart;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.trulycanadian.ExpenseRecorder.Category;
import com.trulycanadian.ExpenseRecorder.ExpenseRecorder;
import com.trulycanadian.ExpenseRecorder.IconMenu;
import com.trulycanadian.ExpenseRecorder.Item;
import com.trulycanadian.ExpenseRecorder.R;
import com.trulycanadian.ExpenseRecorder.ListAdapter.IconMenuAdapter;
import com.trulycanadian.ExpenseRecorder.db.CategoryExpenseDB;
import com.trulycanadian.ExpenseRecorder.db.ItemExpenseDB;

public class DisplayChart extends Activity {
	private static final int PIE_CHART_EXPENSES = 0;
	private static final int CHANGE_DATE = 0;
	PieChart chart;
	Date startDate;
	Date endDate;
	int totcat;
	ArrayList<Double> values = new ArrayList<Double>();

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.mainmenu);
		ExpenseRecorder app = (ExpenseRecorder) getApplication();
		IconMenuAdapter menuItems = new IconMenuAdapter(this);
		ListView listView = (ListView) this.findViewById(R.id.mainMenuList);
		startDate = app.getStartDate();
		endDate = app.getEndDate();
		this.getWindow().setBackgroundDrawableResource(R.drawable.background);
		menuItems.addItem(new IconMenu("Pie Chart Graph", getResources()
				.getDrawable(R.drawable.piechart)));
		listView.setAdapter(menuItems);
		OnItemClickListener listener = new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {

				switch (position) {
				case PIE_CHART_EXPENSES:
					CategorySeries dataSet = loadValues();
					DefaultRenderer renderer = buildCategoryRenderer(randomColor(totcat));
					if (totcat > 0) {
						Intent intent = ChartFactory.getPieChartIntent(DisplayChart.this, dataSet,
								renderer);
						startActivity(intent);
					} else
						finish();
					break;

				}
			}
		};
		listView.setOnItemClickListener(listener);

	}

	CategorySeries loadValues() {
		ItemExpenseDB itemDBHelper;
		CategorySeries series = new CategorySeries("Expenses");

		ArrayList<Item> expenseItems;
		ArrayList<Category> categoriesCredit;
		CategoryExpenseDB categoryDBHelper;
		itemDBHelper = new ItemExpenseDB(this);
		categoryDBHelper = new CategoryExpenseDB(this);
		categoriesCredit = categoryDBHelper.getAllCategory();
		totcat = 0;
		for (Category category : categoriesCredit) {
			expenseItems = itemDBHelper.getItemByDateCategory(category
					.getCatid(), startDate, endDate);
			if (expenseItems.size() > 0) {
				double totalExpenses = 0;
				for (Item item : expenseItems) {
					totalExpenses = +item.getItemAmount();
				}
				System.out.println("Got here Inside loup");
				series.add(category.getName(), totalExpenses);
				totcat = totcat + 1;
			}
		}
		return series;
	}

	protected DefaultRenderer buildCategoryRenderer(int[] colors) {

		DefaultRenderer renderer = new DefaultRenderer();

		for (int color : colors) {

			SimpleSeriesRenderer r = new SimpleSeriesRenderer();

			r.setColor(color);

			renderer.addSeriesRenderer(r);

		}

		return renderer;

	}

	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(0, CHANGE_DATE, 0, R.string.changeDate);
		return true;
	}

	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		case CHANGE_DATE:
			Intent timePeriod = new Intent(this, SetTimePeriodActivity.class);
			this.startActivity(timePeriod);
			return true;
		}
		return super.onMenuItemSelected(featureId, item);

	}

	int[] randomColor(int num) {
		Random randomGenerator = new Random();
		int[] colorsraw = new int[num];
		int i;
		for (i = 0; i < num; i++) {
			System.out.println(randomGenerator.nextInt(255));
			colorsraw[i] = Color.rgb(randomGenerator.nextInt(255),
					randomGenerator.nextInt(255), randomGenerator.nextInt(255));
			System.out.println(colorsraw[i]);
		}
		return colorsraw;
	}
}