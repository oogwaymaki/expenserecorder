package com.trulycanadian.ExpenseRecorder.Activity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.trulycanadian.ExpenseRecorder.Category;
import com.trulycanadian.ExpenseRecorder.ExpenseRecorder;
import com.trulycanadian.ExpenseRecorder.Item;
import com.trulycanadian.ExpenseRecorder.R;
import com.trulycanadian.ExpenseRecorder.db.CategoryExpenseDB;
import com.trulycanadian.ExpenseRecorder.db.ItemExpenseDB;

public class ExpenseCategoryListSummaryActivity extends Activity {
	Date startDate;
	Date endDate;
	ListView categoryListView;
	private static final int CHANGE_DATE = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.listcategorytotal);
		ExpenseRecorder app = (ExpenseRecorder) getApplication();
		startDate = app.getStartDate();
		endDate = app.getEndDate();
		fillDate();
		fillTotalCategoriesAndList();

	}

	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(0, CHANGE_DATE, 0, R.string.changeDate);
		return true;
	}

	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		case CHANGE_DATE:
			Intent timePeriod = new Intent(this, SetTimePeriodActivity.class);
			this.startActivity(timePeriod);
			return true;
		}
		return super.onMenuItemSelected(featureId, item);

	}

	public void onRestart() {
		super.onRestart();
		ExpenseRecorder app = (ExpenseRecorder) getApplication();
		startDate =  app.getStartDate();
		endDate = app.getEndDate();
		fillDate();
		fillTotalCategoriesAndList();
	}

	void fillDate() {
		TextView dateTextFrom = (TextView) this
				.findViewById(R.id.dateTimeTextFrom);
		dateTextFrom.setText(startDate.toLocaleString());
		TextView dateTextTo = (TextView) this.findViewById(R.id.dateTimeTextTo);
		dateTextTo.setText(endDate.toLocaleString());
	}

	void fillTotalCategoriesAndList() {
		ArrayList<HashMap<String, String>> categoryTotalExpenses = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> catExpense;
		SimpleAdapter categoryAdapter;
		categoryListView = (ListView) findViewById(R.id.categoryList);
		DecimalFormat dollarFormat = new DecimalFormat(" ######0.00");
		ArrayList<Category> categoriesCredit;
		CategoryExpenseDB categoryDBHelper;
		ItemExpenseDB itemDBHelper;

		categoryDBHelper = new CategoryExpenseDB(this);

		categoriesCredit = categoryDBHelper.getAllCategory();
		double expenseGrand = 0;
		for (Category category : categoriesCredit) {
			double expenseTotal = 0;

			ArrayList<Item> expenseItems;

			itemDBHelper = new ItemExpenseDB(this);
			expenseItems = itemDBHelper.getItemByDateCategory(category
					.getCatid(), startDate, endDate);
			for (Item item : expenseItems) {
				expenseTotal = item.getItemAmount() + expenseTotal;
			}
			expenseGrand = expenseTotal + expenseGrand;
			catExpense = new HashMap<String, String>();
			catExpense.put("category", category.getName());
			catExpense.put("dollar", dollarFormat.format(expenseTotal));
			catExpense.put("catId", Long.toString(category.getCatid()));
			categoryTotalExpenses.add(catExpense);
			expenseTotal = 0;
		}
		catExpense = new HashMap<String, String>();
		catExpense.put("category", "Grand Total: ");
		catExpense.put("dollar", dollarFormat.format(expenseGrand));
		catExpense.put("catId", "0");
		categoryTotalExpenses.add(catExpense);
		categoryAdapter = new SimpleAdapter(this, categoryTotalExpenses,
				R.layout.columnviewsummary, new String[] { "category",
						"dollar", "catId" }, new int[] { R.id.TITLE_CELL,
						R.id.VALUE_CELL, R.id.dbId });
		categoryListView.setAdapter(categoryAdapter);
		OnItemClickListener listener = new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {

				TextView categoryTextView = (TextView) v
						.findViewById(R.id.dbId);
				Long catId = new Long(categoryTextView.getText().toString());
				Intent showItemIntent = new Intent(
						ExpenseCategoryListSummaryActivity.this,
						ExpenseItemsListSummaryActivity.class);

				if (catId != 0) {
					showItemIntent.putExtra("categoryId", catId);
					showItemIntent.putExtra("startDate", startDate);
					showItemIntent.putExtra("endDate", endDate);
					ExpenseCategoryListSummaryActivity.this
							.startActivity(showItemIntent);
				}

			}
		};
		categoryListView.setOnItemClickListener(listener);
	}
}
