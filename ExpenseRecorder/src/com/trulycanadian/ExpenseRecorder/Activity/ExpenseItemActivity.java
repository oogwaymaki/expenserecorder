package com.trulycanadian.ExpenseRecorder.Activity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.trulycanadian.ExpenseRecorder.Category;
import com.trulycanadian.ExpenseRecorder.Item;
import com.trulycanadian.ExpenseRecorder.R;
import com.trulycanadian.ExpenseRecorder.db.AdapterDB;
import com.trulycanadian.ExpenseRecorder.db.CategoryExpenseDB;
import com.trulycanadian.ExpenseRecorder.db.ItemExpenseDB;

public class ExpenseItemActivity extends Activity {
	/** Called when the activity is first created. */
	private String itemName;
	private Double itemValue;
	private AdapterDB mDbHelper;
	private ItemExpenseDB itemExpenseDB;
	private CategoryExpenseDB categoryDB;
	private Spinner m_myDynamicSpinner;
	private ArrayAdapter<String> m_adapterForSpinner;
	TextView mDateDisplay;
	Button mPickDate;
	EditText itemEditText;
	Button saveItem;
	private int itemBoughtYear;
	private int itemBoughtMonth;
	private int itemBoughtDay;
	static final int DATE_DIALOG_ID = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.credititem);

		mDbHelper = new AdapterDB(this);
		itemExpenseDB = new ItemExpenseDB(this);
		categoryDB = new CategoryExpenseDB(this);
		EditText editView = (EditText) this.findViewById(R.id.creditPrice);
		editView.setText("0.00");

		setupSpinner();
		setupButton();

		final Calendar c = Calendar.getInstance();
		itemBoughtYear = c.get(Calendar.YEAR);
		itemBoughtMonth = c.get(Calendar.MONTH);
		itemBoughtDay = c.get(Calendar.DAY_OF_MONTH);

		// display the current date (this method is below)
		updateDateDisplay();
		OnFocusChangeListener listener = new OnFocusChangeListener() {

			public void onFocusChange(View editBox, boolean arg1) {
				EditText editFocus = (EditText) ExpenseItemActivity.this
						.findViewById(R.id.creditPrice);
				if (arg1 == false) {
					Log.w("Focus:", "Got here");
					System.out.println(editFocus.getText().toString());
					if (!(editFocus.getText().toString()
							.matches("^[0-9]+[.][0-9][0-9]$"))) {
						System.out.println("FAILED");
						AlertDialog.Builder builder = new AlertDialog.Builder(
								ExpenseItemActivity.this);
						builder.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int item) {
									}
								});
						builder
								.setMessage("Please enter correct dollar amount");
						AlertDialog alert = builder.create();
						alert.show();
						editFocus.setText("0.00");
					}
				}
			}
		};

		editView.setOnFocusChangeListener(listener);

	}

	private void setupButton() {

		mDateDisplay = (TextView) findViewById(R.id.dateDisplay);
		saveItem = (Button) findViewById(R.id.saveButton);
		mPickDate = (Button) findViewById(R.id.pickDate);
		saveItem.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Spinner spinner = (Spinner) findViewById(R.id.spinner);
				String categoryName = spinner.getSelectedItem().toString();
				Category category = categoryDB.getCategory(categoryName);
				itemEditText = (EditText) findViewById(R.id.creditItem);
				EditText itemValueEditText = (EditText) findViewById(R.id.creditPrice);
				String itemValueString = itemValueEditText.getText().toString();
				itemName = itemEditText.getText().toString();
				itemValue = new Double(itemValueString);
				Item expenseItem = new Item();
				Date date = new Date();
				date.setYear(itemBoughtYear - 1900);
				date.setMonth(itemBoughtMonth);
				date.setDate(itemBoughtDay);
				Log.w("TEST", expenseItem.getItemName() + " "
						+ expenseItem.getCatId());
				expenseItem.setDate(date);
				expenseItem.setCatId(category.getCatid());
				System.out.println(itemName);
				System.out.println(itemValue);
				expenseItem.setItemName(itemName);
				expenseItem.setItemAmount(itemValue);
			
				if (itemName.trim().equals(""))
					callError("Item name can not be blank");
				else
				{
					itemExpenseDB.save(expenseItem);
					finish();
				}
			}
		}); // add a click listener to the button
		mPickDate.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showDialog(DATE_DIALOG_ID);
			}
		});
	}

	private void setupSpinner() {
		ArrayList<String> categoriesCredit;

		m_myDynamicSpinner = (Spinner) findViewById(R.id.spinner);
		m_adapterForSpinner = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item);
		m_adapterForSpinner
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		m_myDynamicSpinner.setAdapter(m_adapterForSpinner);
		mDbHelper.open();
		categoriesCredit = mDbHelper.selectAllCategoryCredit();
		mDbHelper.close();
		for (String category : categoriesCredit)
			m_adapterForSpinner.add(category);
	}

	private void updateDateDisplay() {
		mDateDisplay.setText(new StringBuilder()
		// Month is 0 based so add 1
				.append("Date Purchased: ").append(itemBoughtMonth + 1).append(
						"-").append(itemBoughtDay).append("-").append(
						itemBoughtYear).append(" "));
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			itemBoughtYear = year;
			itemBoughtMonth = monthOfYear;
			itemBoughtDay = dayOfMonth;
			updateDateDisplay();
		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, mDateSetListener, itemBoughtYear,
					itemBoughtMonth, itemBoughtDay);
		}
		return null;
	}

	void callError(String entry) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
			}
		});
		builder.setMessage(entry);
		AlertDialog alert = builder.create();
		alert.show();
	}
}