package com.trulycanadian.ExpenseRecorder.Activity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.trulycanadian.ExpenseRecorder.Category;
import com.trulycanadian.ExpenseRecorder.Item;
import com.trulycanadian.ExpenseRecorder.R;
import com.trulycanadian.ExpenseRecorder.db.AdapterDB;
import com.trulycanadian.ExpenseRecorder.db.CategoryExpenseDB;
import com.trulycanadian.ExpenseRecorder.db.ItemExpenseDB;

public class ExpenseItemEditActivity extends Activity {

	/** Called when the activity is first created. */
	private String itemName;
	private Double itemValue;
	private AdapterDB mDbHelper;
	private ItemExpenseDB itemExpenseDB;
	private CategoryExpenseDB categoryDB;
	private Spinner m_myDynamicSpinner;
	private ArrayAdapter<String> m_adapterForSpinner;
	private HashMap<String, Integer> spinnerHash = new HashMap<String, Integer>();
	TextView mDateDisplay;
	Button mPickDate;
	EditText itemEditText;
	Button saveItem;
	private int itemBoughtYear;
	private int itemBoughtMonth;
	private int itemBoughtDay;
	private Long itemId;
	static final int DATE_DIALOG_ID = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.credititem);

		mDbHelper = new AdapterDB(this);
		itemExpenseDB = new ItemExpenseDB(this);
		categoryDB = new CategoryExpenseDB(this);
		Bundle extras = getIntent().getExtras();
		itemId = extras.getLong("itemId");

		setupSpinner();
		setupButton();

		final Calendar c = Calendar.getInstance();
		itemBoughtYear = c.get(Calendar.YEAR);
		itemBoughtMonth = c.get(Calendar.MONTH);
		itemBoughtDay = c.get(Calendar.DAY_OF_MONTH);
		EditText editView = (EditText) this.findViewById(R.id.creditPrice);
		editView.setText("0.00");
		// display the current date (this method is below)
		loadItem(itemId);
		updateDateDisplay();
		OnFocusChangeListener listener = new OnFocusChangeListener() {

			public void onFocusChange(View editBox, boolean arg1) {
				EditText editFocus = (EditText) ExpenseItemEditActivity.this
						.findViewById(R.id.creditPrice);
				if (arg1 == false) {
					if (!(editFocus.getText().toString()
							.matches("^[0-9]+[.][0-9][0-9]$"))) {
						System.out.println("FAILED");
						AlertDialog.Builder builder = new AlertDialog.Builder(
								ExpenseItemEditActivity.this);
						builder.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int item) {
									}
								});
						builder
								.setMessage("Please enter correct dollar amount");
						AlertDialog alert = builder.create();
						alert.show();
						editFocus.setText("0.00");
					}
				}
			}
		};

		editView.setOnFocusChangeListener(listener);

	}

	private void loadItem(Long id) {
		ItemExpenseDB itemExpenseLoader = new ItemExpenseDB(this);
		Item expenseItem;
		DecimalFormat dollarFormat = new DecimalFormat(" ######0.00");
		expenseItem = itemExpenseLoader.getItem(id);
		EditText itemEditText = (EditText) findViewById(R.id.creditItem);
		EditText itemValueEditText = (EditText) findViewById(R.id.creditPrice);
		itemEditText.setText(expenseItem.getItemName());
		itemValueEditText.setText(dollarFormat.format(expenseItem.getItemAmount()));
		m_myDynamicSpinner.setSelection(spinnerHash.get(expenseItem
				.getCategory()));
		this.itemBoughtYear = expenseItem.getDate().getYear() + 1900;
		this.itemBoughtMonth = expenseItem.getDate().getMonth();
		this.itemBoughtDay = expenseItem.getDate().getDate();
	}

	private void setupButton() {

		mDateDisplay = (TextView) findViewById(R.id.dateDisplay);
		saveItem = (Button) findViewById(R.id.saveButton);
		mPickDate = (Button) findViewById(R.id.pickDate);
		saveItem.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Spinner spinner = (Spinner) findViewById(R.id.spinner);
				String categoryName = spinner.getSelectedItem().toString();
				Category category = categoryDB.getCategory(categoryName);
				itemEditText = (EditText) findViewById(R.id.creditItem);
				EditText itemValueEditText = (EditText) findViewById(R.id.creditPrice);
				String itemValueString = itemValueEditText.getText().toString();
				itemName = itemEditText.getText().toString();
				itemValue = new Double(itemValueString);
				Item expenseItem = new Item();
				Date date = new Date();
				date.setYear(itemBoughtYear - 1900);
				date.setMonth(itemBoughtMonth);
				date.setDate(itemBoughtDay);
				expenseItem.setDate(date);
				expenseItem.setId(itemId);
				expenseItem.setCatId(category.getCatid());
				expenseItem.setItemName(itemName);
				expenseItem.setItemAmount(itemValue);
				if (itemName.trim().equals(""))
					callError("Item Name can not be blank");
				else
				{
					itemExpenseDB.updateItem(expenseItem);
					finish();
				}
			}
		}); // add a click listener to the button
		mPickDate.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showDialog(DATE_DIALOG_ID);
			}
		});
	}

	private void setupSpinner() {
		ArrayList<String> categoriesCredit;

		m_myDynamicSpinner = (Spinner) findViewById(R.id.spinner);
		m_adapterForSpinner = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item);
		m_adapterForSpinner
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		m_myDynamicSpinner.setAdapter(m_adapterForSpinner);
		mDbHelper.open();
		categoriesCredit = mDbHelper.selectAllCategoryCredit();
		mDbHelper.close();
		int count = 0;
		for (String category : categoriesCredit) {
			spinnerHash.put(category, count);
			m_adapterForSpinner.add(category);
			count = count + 1;
		}
	}

	private void updateDateDisplay() {
		mDateDisplay.setText(new StringBuilder()
		// Month is 0 based so add 1
				.append("Date Purchased: ").append(itemBoughtMonth + 1).append(
						"-").append(itemBoughtDay).append("-").append(
						itemBoughtYear).append(" "));
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			itemBoughtYear = year;
			itemBoughtMonth = monthOfYear;
			itemBoughtDay = dayOfMonth;
			updateDateDisplay();
		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, mDateSetListener, itemBoughtYear,
					itemBoughtMonth, itemBoughtDay);
		}
		return null;

	}

	void callError(String entry) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
			}
		});
		builder.setMessage(entry);
		AlertDialog alert = builder.create();
		alert.show();
	}
}