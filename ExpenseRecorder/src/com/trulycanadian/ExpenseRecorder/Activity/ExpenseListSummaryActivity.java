package com.trulycanadian.ExpenseRecorder.Activity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.trulycanadian.ExpenseRecorder.Category;
import com.trulycanadian.ExpenseRecorder.Item;
import com.trulycanadian.ExpenseRecorder.R;
import com.trulycanadian.ExpenseRecorder.db.CategoryExpenseDB;
import com.trulycanadian.ExpenseRecorder.db.ItemExpenseDB;

public class ExpenseListSummaryActivity extends Activity {
	TableLayout tl;
	private CategoryExpenseDB categoryDBHelper;
	private ItemExpenseDB itemDBHelper;
	int zero = 00;
	int FF = 255;
	private Date startDate;
	private Date endDate;
	@Override
	public void onCreate(Bundle savedInstanceState) {
    
		
		super.onCreate(savedInstanceState);
        setContentView(R.layout.listsummary);
        
        /* Find Tablelayout defined in main.xml */
        tl = (TableLayout)findViewById(R.id.myTableLayout);
             /* Create a new row to be added. */
        Bundle extras = getIntent().getExtras();
        startDate = (Date) extras.get("startDate");
        endDate = (Date) extras.get("endDate");
       
        fillTotalCategories();
         
        /* Add row to TableLayout. */
        
    }
	
    	void fillTotalCategories()
    	{
    		DecimalFormat dollarFormat = new DecimalFormat(" ######0.00");
    		ArrayList<Category> categoriesCredit;
    		
            categoryDBHelper = new CategoryExpenseDB(this);
            
            categoriesCredit = categoryDBHelper.getAllCategory();
            double expenseGrand = 0;
            for ( Category category : categoriesCredit )
            {
            	double expenseTotal = 0;
            	
            	ArrayList<Item> expenseItems;
            	
            	TableRow tr = new TableRow(this);
                tr.setLayoutParams(new LayoutParams(
                               LayoutParams.FILL_PARENT,
                               LayoutParams.WRAP_CONTENT));
            	Log.v("Check",category.getName());
            	
            	TextView addColumn = new TextView(this);
            	itemDBHelper = new  ItemExpenseDB(this);
            	expenseItems = itemDBHelper.getItemByDateCategory(category.getCatid(),startDate,endDate);
            	for (Item item : expenseItems)
            	{
           	     	expenseTotal = item.getItemAmount() + expenseTotal;
            	}
            	expenseGrand = expenseTotal + expenseGrand;
            	            	addColumn.setText("Total " + category.getName() + ":");
            	addColumn.setTextColor(Color.rgb(zero, FF,zero ));
            	addColumn.setTextSize(20);
            	tr.addView(addColumn);
            	addColumn = null;
            	addColumn = new TextView(this);
            	addColumn.setText(dollarFormat.format(expenseTotal));
            	addColumn.setTextColor(Color.rgb(zero,zero,FF));
            	addColumn.setTextSize(20);
            	tr.addView(addColumn,1);
            	tl.addView(tr,new TableLayout.LayoutParams(
                        LayoutParams.FILL_PARENT,
                        LayoutParams.WRAP_CONTENT));
            	expenseTotal = 0;
            }
            TextView grandTotal = (TextView) findViewById(R.id.grandTotal);
            grandTotal.setText(dollarFormat.format(expenseGrand));
    	}
}
