package com.trulycanadian.ExpenseRecorder.Activity;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;

import com.trulycanadian.ExpenseRecorder.Category;
import com.trulycanadian.ExpenseRecorder.ExpenseRecorder;
import com.trulycanadian.ExpenseRecorder.Item;
import com.trulycanadian.ExpenseRecorder.R;
import com.trulycanadian.ExpenseRecorder.db.CategoryExpenseDB;
import com.trulycanadian.ExpenseRecorder.db.CategoryIncomeDB;
import com.trulycanadian.ExpenseRecorder.db.ItemExpenseDB;
import com.trulycanadian.ExpenseRecorder.db.ItemIncomeDB;

;

public class ExportCsvActivity extends Activity {
	EditText fileName;
	Date startDate;
	Date endDate;
	private Thread loadButton;
	private ProgressDialog loading;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.exportcsv);
		fileName = (EditText) this.findViewById(R.id.fileName);
		fileName.setText("/sdcard/");
		ExpenseRecorder app = (ExpenseRecorder) getApplication();
		startDate = app.getStartDate();
		endDate = app.getEndDate();
		setupButton();
		OnFocusChangeListener listener = new OnFocusChangeListener() {

			public void onFocusChange(View editBox, boolean arg1) {
				EditText editFocus = (EditText) ExportCsvActivity.this
						.findViewById(R.id.fileName);
				if (arg1 == false) {
					Log.w("Focus:", "Got here");
					System.out.println(editFocus.getText().toString());
					if (!(editFocus.getText().toString()
							.matches("/sdcard/[a-zA-Z.0-9]+"))) {
						System.out.println("FAILED");
						AlertDialog.Builder builder = new AlertDialog.Builder(
								ExportCsvActivity.this);
						builder.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int item) {
									}
								});
						builder.setMessage("You must save with proper name on /sdcard/");
						AlertDialog alert = builder.create();
						alert.show();
						editFocus.setText("/sdcard/");
					}
				}
			}
		};
		fileName.setOnFocusChangeListener(listener);
	}

	private void setupButton() {
		Button saveItem = (Button) findViewById(R.id.saveFile);

		saveItem.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				boolean successexpense = true;
				boolean successincome = true;
				if (!(fileName.getText().toString().trim()
						.matches("/sdcard/[a-zA-Z.0-9]+"))) {
					fileName.setText("/sdcard/");
					callError("You must save with proper name on /sdcard/");
				} else {

					loading = ProgressDialog.show(ExportCsvActivity.this,
							"Saving...", "Saving " + fileName.getText().toString());

					loadButton = new Thread() {
						public void run() {
							boolean successexpense = true;
							boolean successincome = true;
							successexpense = saveExpenseFileOnSdCard();
							if (successexpense)
								callError("Expense file could not be saved");
							successincome = saveIncomeFileOnSdCard();
							if (successincome)
								callError("Income file could not be saved");
							handler.sendEmptyMessage(0);
							if (!successincome && !successexpense)
								finish();
						}
					};
					loadButton.start();

			
					
				
				}
			}
		});
	}
	private Handler handler = new Handler() {

		public void handleMessage(Message m) {

			super.handleMessage(m);
			try {
				loadButton.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("got here");
			// add PayPal button to the Activity layout
			loading.dismiss();
		}
	};

	private boolean saveIncomeFileOnSdCard() {
		StringBuilder output;
		boolean error = false;
		output = exportIncomeTotalCategoriesAndList();
		boolean canWrite = checkSdCard();

		if (canWrite) {
			Writer writer = null;
			try {
				File file = new File(fileName.getText().toString()
						+ "income.csv");
				writer = new BufferedWriter(new FileWriter(file));
				writer.write(output.toString());
			} catch (Exception e) {
				error = true;
				e.printStackTrace();
			} finally {
				try {
					if (writer != null) {
						writer.close();
					}
				} catch (Exception e) {
					error = true;
					e.printStackTrace();
				}

			}
		} else {
			callError("Sorry the sdcard is not in a writeable state");
			error = true;
		}
		return error;
	}

	private boolean checkSdCard() {
		boolean canWrite = true;
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// We can read and write the media
			canWrite = true;
		} else {
			// Something else is wrong. It may be one of many other states, but
			// all we need
			// to know is we can neither read nor write
			canWrite = false;

		}
		return canWrite;
	}

	private boolean saveExpenseFileOnSdCard() {
		StringBuilder output;
		output = exportExpenseTotalCategoriesAndList();
		boolean error = false;
		boolean canWrite = checkSdCard();
		if (canWrite) {
			Writer writer = null;
			try {
				File file = new File(fileName.getText().toString()
						+ "expense.csv");
				writer = new BufferedWriter(new FileWriter(file));
				writer.write(output.toString());
			} catch (Exception e) {
				error = true;
				e.printStackTrace();
			} finally {
				try {
					if (writer != null) {
						writer.close();
					}
				} catch (Exception e) {
					error = true;
					e.printStackTrace();
				}

			}
		} else {
			callError("Sorry the sdcard is not in a writeable state");
			error = true;
		}
		return error;
	}

	private StringBuilder exportExpenseTotalCategoriesAndList() {

		ArrayList<Category> categoriesCredit;
		CategoryExpenseDB categoryDBHelper;
		ItemExpenseDB itemDBHelper;
		StringBuilder output = new StringBuilder();
		DecimalFormat dollarFormat = new DecimalFormat(" ######0.00");
		categoryDBHelper = new CategoryExpenseDB(this);
		double expenseTotal = 0;
		categoriesCredit = categoryDBHelper.getAllCategory();
		output.append("Category ID,Item ID,Date,Category Name,Item Name, Item Amount\n");
		for (Category category : categoriesCredit) {

			ArrayList<Item> expenseItems;

			itemDBHelper = new ItemExpenseDB(this);
			expenseItems = itemDBHelper.getItemByDateCategory(
					category.getCatid(), startDate, endDate);

			for (Item item : expenseItems) {
				expenseTotal = item.getItemAmount() + expenseTotal;

				output.append(item.getCatId().toString() + ","
						+ item.getId().toString() + ","
						+ item.getDate().toString() + "," + category.getName()
						+ "," + item.getItemName() + "," + dollarFormat.format(item.getItemAmount())
						+ "\n");
			}

		}
		output.append("\nExpense Total = " + expenseTotal + "\n");
		return output;
	}

	private StringBuilder exportIncomeTotalCategoriesAndList() {

		ArrayList<Category> categoriesCredit;
		CategoryIncomeDB categoryDBHelper;
		ItemIncomeDB itemDBHelper;
		DecimalFormat dollarFormat = new DecimalFormat(" ######0.00");
		StringBuilder output = new StringBuilder();
		categoryDBHelper = new CategoryIncomeDB(this);

		categoriesCredit = categoryDBHelper.getAllCategory();
		output.append("Category ID,Item ID,Date,Category Name,Item Name, Item Amount\n");
		double incomeTotal = 0;
		for (Category category : categoriesCredit) {

			Log.w("Inside CAtegory", category.getName());
			ArrayList<Item> incomeItems;

			itemDBHelper = new ItemIncomeDB(this);
			incomeItems = itemDBHelper.getItemByDateCategory(
					category.getCatid(), startDate, endDate);
			Log.w("startdate", startDate.toString());
			Log.w("enddate", endDate.toString());

			for (Item item : incomeItems) {
				Log.w("Inside Item", item.getItemName());
				incomeTotal = item.getItemAmount() + incomeTotal;

				output.append(item.getCatId().toString() + ","
						+ item.getId().toString() + ","
						+ item.getDate().toString() + "," + category.getName()
						+ "," + item.getItemName() + "," + dollarFormat.format(item.getItemAmount()) + "\n");

			}

		}
		output.append("\nIncome Total = " + incomeTotal + "\n");
		return output;
	}

	void callError(String entry) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
			}
		});
		builder.setMessage(entry);
		AlertDialog alert = builder.create();
		alert.show();
	}
}
