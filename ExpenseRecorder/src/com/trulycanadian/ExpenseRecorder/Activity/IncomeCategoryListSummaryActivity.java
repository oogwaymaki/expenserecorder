package com.trulycanadian.ExpenseRecorder.Activity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.trulycanadian.ExpenseRecorder.Category;
import com.trulycanadian.ExpenseRecorder.ExpenseRecorder;
import com.trulycanadian.ExpenseRecorder.Item;
import com.trulycanadian.ExpenseRecorder.R;
import com.trulycanadian.ExpenseRecorder.db.CategoryIncomeDB;
import com.trulycanadian.ExpenseRecorder.db.ItemIncomeDB;

public class IncomeCategoryListSummaryActivity  extends Activity {
	Date startDate;
	Date endDate;
	ListView categoryListView;
	private static final int CHANGE_DATE=0;
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.listcategorytotal);	
		ExpenseRecorder app = (ExpenseRecorder) getApplication();
		startDate = app.getStartDate();
		endDate = app.getEndDate();

		/* Add row to TableLayout. */
		fillDate();
		fillTotalCategoriesAndList();
	}
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(0, CHANGE_DATE, 0, R.string.changeDate);
		return true;
	}
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		case CHANGE_DATE:
			Intent timePeriod = new Intent(this, SetTimePeriodActivity.class);
			this.startActivity(timePeriod);
			return true;
		}
		return super.onMenuItemSelected(featureId, item);

	}

	void fillDate()
	{
		TextView dateTextFrom = (TextView)  this.findViewById(R.id.dateTimeTextFrom);
		dateTextFrom.setText(startDate.toLocaleString());
		TextView dateTextTo = (TextView) this.findViewById(R.id.dateTimeTextTo);
		dateTextTo.setText(endDate.toLocaleString());
	}
	public void onRestart()
	{
		super.onRestart();
		ExpenseRecorder app = (ExpenseRecorder) getApplication();
		startDate = app.getStartDate();
		endDate = app.getEndDate();
		fillDate();
		fillTotalCategoriesAndList();
	}

	void fillTotalCategoriesAndList() {
		ArrayList<HashMap<String, String>> categoryTotalIncomes = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> catIncome;
		SimpleAdapter categoryAdapter;
		categoryListView = (ListView) findViewById(R.id.categoryList);
		DecimalFormat dollarFormat = new DecimalFormat(" ######0.00");
		ArrayList<Category> categoriesDebit;
		CategoryIncomeDB categoryDBHelper;
		ItemIncomeDB itemDBHelper;

		categoryDBHelper = new CategoryIncomeDB(this);

		categoriesDebit = categoryDBHelper.getAllCategory();
		double incomeGrand = 0;
		for (Category category : categoriesDebit) {
			double incomeTotal = 0;

			ArrayList<Item> incomeItems;

			itemDBHelper = new ItemIncomeDB(this);
			incomeItems = itemDBHelper.getItemByDateCategory(
					category.getCatid(), startDate, endDate);
			for (Item item : incomeItems) {
				incomeTotal = item.getItemAmount() + incomeTotal;
			}
			incomeGrand = incomeTotal + incomeGrand;
			catIncome = new HashMap<String, String>();
			catIncome.put("category", category.getName());
			catIncome.put("dollar", dollarFormat.format(incomeTotal));
			catIncome.put("catId",Long.toString(category.getCatid()));
			categoryTotalIncomes.add(catIncome);
			incomeTotal = 0;
		}
		catIncome = new HashMap<String, String>();
		catIncome.put("category", "Grand Total: ");
		catIncome.put("dollar", dollarFormat.format(incomeGrand));
		catIncome.put("catId", "0");
		categoryTotalIncomes.add(catIncome);
		categoryAdapter = new SimpleAdapter(this, categoryTotalIncomes,
				R.layout.columnviewsummary,
				new String[] { "category", "dollar" ,"catId"}, new int[] {
						R.id.TITLE_CELL, R.id.VALUE_CELL ,R.id.dbId});
		categoryListView.setAdapter(categoryAdapter);
		OnItemClickListener listener = new OnItemClickListener()
		{
			 public void onItemClick(AdapterView parent, View v, int position, long id)
			    {

				            TextView categoryTextView = (TextView) v.findViewById(R.id.dbId);
				            Long catId = new Long(categoryTextView.getText().toString());
							Intent showItemIntent = new Intent(IncomeCategoryListSummaryActivity.this, IncomeItemsListSummaryActivity.class);
						
							if (catId != 0)
							{
								showItemIntent.putExtra("categoryId", catId);
								IncomeCategoryListSummaryActivity.this.startActivity(showItemIntent); 
							}
							        
			    }
		};
		categoryListView.setOnItemClickListener(listener);
	}
}
