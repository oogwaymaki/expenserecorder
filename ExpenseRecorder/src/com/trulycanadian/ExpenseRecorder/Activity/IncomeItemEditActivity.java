package com.trulycanadian.ExpenseRecorder.Activity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.trulycanadian.ExpenseRecorder.Category;
import com.trulycanadian.ExpenseRecorder.Item;
import com.trulycanadian.ExpenseRecorder.R;
import com.trulycanadian.ExpenseRecorder.db.AdapterDB;
import com.trulycanadian.ExpenseRecorder.db.CategoryIncomeDB;
import com.trulycanadian.ExpenseRecorder.db.ItemIncomeDB;

public class IncomeItemEditActivity extends Activity {

	/** Called when the activity is first created. */
	private String itemName;
	private Double itemValue;
	private Spinner m_myDynamicSpinner;
	private ArrayAdapter<String> m_adapterForSpinner;
	private HashMap<String, Integer> spinnerHash = new HashMap<String, Integer>();
	TextView mDateDisplay;
	EditText itemEditText;
	Button saveItem;
	Button mPickDate;
	private int itemBoughtYear;
	private int itemBoughtMonth;
	private int itemBoughtDay;
	private Long itemId;
	static final int DATE_DIALOG_ID = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.debititem);
		Bundle extras = getIntent().getExtras();
		itemId = extras.getLong("itemId");

		setupSpinner();
		setupButton();

		final Calendar c = Calendar.getInstance();
		itemBoughtYear = c.get(Calendar.YEAR);
		itemBoughtMonth = c.get(Calendar.MONTH);
		itemBoughtDay = c.get(Calendar.DAY_OF_MONTH);
		EditText editView = (EditText) this.findViewById(R.id.incomePrice);
		editView.setText("0.00");
		// display the current date (this method is below)
		Log.w("Inside onCreate", itemId.toString());
		loadItem(itemId);
		updateDateDisplay();
		OnFocusChangeListener listener = new OnFocusChangeListener() {

			public void onFocusChange(View editBox, boolean arg1) {
				EditText editFocus = (EditText) IncomeItemEditActivity.this
						.findViewById(R.id.incomePrice);
				if (arg1 == false) {
					if (!(editFocus.getText().toString()
							.matches("^[0-9]+[.][0-9][0-9]$"))) {
						System.out.println("FAILED");
						AlertDialog.Builder builder = new AlertDialog.Builder(
								IncomeItemEditActivity.this);
						builder.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int item) {
									}
								});
						builder
								.setMessage("Please enter correct dollar amount");
						AlertDialog alert = builder.create();
						alert.show();
						editFocus.setText("0.00");
					}
				}
			}
		};

		editView.setOnFocusChangeListener(listener);

	}

	private void loadItem(Long id) {
		ItemIncomeDB itemIncomeLoader = new ItemIncomeDB(this);
		Item incomeItem;
		DecimalFormat dollarFormat = new DecimalFormat(" ######0.00");
		Log.w("Inside Load Item", id.toString());
		incomeItem = itemIncomeLoader.getItem(id);
		Log.w("INCOME ITEM", incomeItem.getItemName());
		EditText itemEditText = (EditText) findViewById(R.id.incomeItem);
		EditText itemValueEditText = (EditText) findViewById(R.id.incomePrice);
		itemEditText.setText(incomeItem.getItemName());
		itemValueEditText.setText(dollarFormat.format(incomeItem.getItemAmount()));
		Log.w("HASH:", incomeItem.getCategory());

		m_myDynamicSpinner.setSelection(spinnerHash.get(incomeItem
				.getCategory()));
		this.itemBoughtYear = incomeItem.getDate().getYear() + 1900;
		this.itemBoughtMonth = incomeItem.getDate().getMonth();
		this.itemBoughtDay = incomeItem.getDate().getDate();
	}

	private void setupButton() {

		mDateDisplay = (TextView) findViewById(R.id.dateDisplay);
		saveItem = (Button) findViewById(R.id.saveButton);
		mPickDate = (Button) findViewById(R.id.pickDate);
		saveItem.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Spinner spinner = (Spinner) findViewById(R.id.spinner);
				String categoryName = spinner.getSelectedItem().toString();
				CategoryIncomeDB categoryDB = new CategoryIncomeDB(
						IncomeItemEditActivity.this);
				Category category = categoryDB.getCategory(categoryName);
				itemEditText = (EditText) findViewById(R.id.incomeItem);
				EditText itemValueEditText = (EditText) findViewById(R.id.incomePrice);
				String itemValueString = itemValueEditText.getText().toString();
				itemName = itemEditText.getText().toString();
				itemValue = new Double(itemValueString);
				Item incomeItem = new Item();
				Date date = new Date();
				date.setYear(itemBoughtYear - 1900);
				date.setMonth(itemBoughtMonth);
				date.setDate(itemBoughtDay);
				incomeItem.setDate(date);
				incomeItem.setId(itemId);
				incomeItem.setCatId(category.getCatid());
				incomeItem.setItemName(itemName);
				incomeItem.setItemAmount(itemValue);
				ItemIncomeDB itemIncomeUpdater = new ItemIncomeDB(
						IncomeItemEditActivity.this);
				if (itemName.trim().equals(""))
					callError("Item name can not be blank");
				else {
					itemIncomeUpdater.updateItem(incomeItem);
					finish();
				}

			}
		}); // add a click listener to the button
		mPickDate.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showDialog(DATE_DIALOG_ID);
			}
		});
	}

	private void setupSpinner() {
		ArrayList<String> categoriesDebit;

		m_myDynamicSpinner = (Spinner) findViewById(R.id.spinner);
		m_adapterForSpinner = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item);
		m_adapterForSpinner
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		m_myDynamicSpinner.setAdapter(m_adapterForSpinner);
		AdapterDB mDbHelper = new AdapterDB(IncomeItemEditActivity.this);
		mDbHelper.open();
		categoriesDebit = mDbHelper.selectAllCategoryDebit();
		mDbHelper.close();
		int count = 0;
		for (String category : categoriesDebit) {
			spinnerHash.put(category, count);
			m_adapterForSpinner.add(category);
			count = count + 1;
		}
	}

	private void updateDateDisplay() {
		mDateDisplay.setText(new StringBuilder()
		// Month is 0 based so add 1
				.append("Date Purchased: ").append(itemBoughtMonth + 1).append(
						"-").append(itemBoughtDay).append("-").append(
						itemBoughtYear).append(" "));
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			itemBoughtYear = year;
			itemBoughtMonth = monthOfYear;
			itemBoughtDay = dayOfMonth;
			updateDateDisplay();
		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, mDateSetListener, itemBoughtYear,
					itemBoughtMonth, itemBoughtDay);
		}
		return null;
	}
	void callError(String entry) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
			}
		});
		builder.setMessage(entry);
		AlertDialog alert = builder.create();
		alert.show();
	}
}
