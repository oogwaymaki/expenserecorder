package com.trulycanadian.ExpenseRecorder.Activity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.AdapterView.AdapterContextMenuInfo;

import com.trulycanadian.ExpenseRecorder.ExpenseRecorder;
import com.trulycanadian.ExpenseRecorder.Item;
import com.trulycanadian.ExpenseRecorder.R;
import com.trulycanadian.ExpenseRecorder.ListAdapter.CustomListMenuAdapter;
import com.trulycanadian.ExpenseRecorder.db.ItemIncomeDB;

public class IncomeItemsListSummaryActivity extends Activity {

	Date startDate;
	Date endDate;
	Long catId;

	HashMap<Long, Long> listMap = new HashMap<Long, Long>();

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.listitemtotal);
		ExpenseRecorder app = (ExpenseRecorder) getApplication();
		startDate = app.getStartDate();
		endDate = app.getEndDate();

		ListView itemListView = (ListView) findViewById(R.id.itemViewList);
		Bundle extras = getIntent().getExtras();

		catId = extras.getLong("categoryId");
		fillTotalCategoriesAndList();
		registerForContextMenu(itemListView);
		/* Add row to TableLayout. */

	}

	public void onRestart() {
		super.onRestart();
		setContentView(R.layout.listitemtotal);
		ListView itemListView = (ListView) findViewById(R.id.itemViewList);
		ExpenseRecorder app = (ExpenseRecorder) getApplication();
		startDate = app.getStartDate();
		endDate = app.getEndDate();

		Bundle extras = getIntent().getExtras();
		catId = extras.getLong("categoryId");
		fillTotalCategoriesAndList();
		registerForContextMenu(itemListView);

	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.add(0, 0, 0, "Edit Item");
		menu.add(0, 0, 0, "Delete Item");

	}

	public boolean onContextItemSelected(MenuItem item) {

		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();

		if (item.getTitle() == "Delete Item")
			deleteItem(listMap.get(info.id));
		if (item.getTitle() == "Edit Item")
			editItem(listMap.get(info.id));
		return super.onContextItemSelected(item);
	}

	public void editItem(Long itemId) {
		Intent editItemActivity = new Intent(this, IncomeItemEditActivity.class);
		editItemActivity.putExtra("itemId", itemId);
		Log.w("Inside editItem ", itemId.toString());
		this.startActivity(editItemActivity);
	}

	public void deleteItem(final Long itemId) {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				IncomeItemsListSummaryActivity.this);
		builder.setMessage("Are you sure you want to delete item?")
				.setCancelable(false).setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								ItemIncomeDB itemIncomeDeleter = new ItemIncomeDB(
										IncomeItemsListSummaryActivity.this);
								itemIncomeDeleter.deleteItem(itemId);
								fillTotalCategoriesAndList();
							}
						}).setNegativeButton("No",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
								fillTotalCategoriesAndList();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	}

	void fillTotalCategoriesAndList() {
		ArrayList<HashMap<String, String>> incomeTotalExpenses = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> catIncome;
		CustomListMenuAdapter categoryAdapter;
		ListView itemListView = (ListView) findViewById(R.id.itemViewList);
		DecimalFormat dollarFormat = new DecimalFormat(" ######0.00");
		ItemIncomeDB itemDBHelper;

		double incomeTotal = 0;

		ArrayList<Item> incomeItems;

		itemDBHelper = new ItemIncomeDB(this);
		incomeItems = itemDBHelper
				.getItemByDateCategory(catId, startDate, endDate);
		Long count = 0L;
		for (Item item : incomeItems) {
			listMap.put(count, item.getId());
			incomeTotal = item.getItemAmount() + incomeTotal;

			catIncome = new HashMap<String, String>();
			catIncome.put("item", item.getItemName());
			catIncome.put("dollar", dollarFormat.format(item.getItemAmount()));
			catIncome.put("itemId", Long.toString(item.getId()));
			catIncome.put("date", item.getStringDate());
			Log.w("ITEM:", item.getItemName());
			incomeTotalExpenses.add(catIncome);
			count = count + 1;
		}

		catIncome = new HashMap<String, String>();
		catIncome.put("item", "Grand Total: ");
		catIncome.put("dollar", dollarFormat.format(incomeTotal));
		catIncome.put("itemId", "0");
		incomeTotalExpenses.add(catIncome);
		categoryAdapter = new CustomListMenuAdapter(this, incomeTotalExpenses,
				R.layout.columnviewsummary, new String[] { "item", "dollar",
						"itemId", "date" }, new int[] { R.id.TITLE_CELL,
						R.id.VALUE_CELL, R.id.dbId, R.id.TITLE_DATE });
		itemListView.setAdapter(categoryAdapter);
	}
}