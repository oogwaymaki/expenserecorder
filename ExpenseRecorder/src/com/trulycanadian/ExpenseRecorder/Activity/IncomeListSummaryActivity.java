package com.trulycanadian.ExpenseRecorder.Activity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.trulycanadian.ExpenseRecorder.Category;
import com.trulycanadian.ExpenseRecorder.Item;
import com.trulycanadian.ExpenseRecorder.R;
import com.trulycanadian.ExpenseRecorder.db.CategoryIncomeDB;
import com.trulycanadian.ExpenseRecorder.db.ItemIncomeDB;

public class IncomeListSummaryActivity extends Activity {

	TableLayout tl;
	private CategoryIncomeDB categoryDBHelper;
	private ItemIncomeDB itemDBHelper;
	int zero = 00;
	int FF = 255;
	Date startDate;
	Date endDate;
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.listsummary);
        Bundle extras = getIntent().getExtras();
        startDate = (Date) extras.get("startDate");
        endDate = (Date) extras.get("endDate");
		/* Find Tablelayout defined in main.xml */
		tl = (TableLayout) findViewById(R.id.myTableLayout);
		/* Create a new row to be added. */
		fillTotalCategories();
		/* Add row to TableLayout. */

	}

	void fillTotalCategories() {

		DecimalFormat dollarFormat = new DecimalFormat(" ######0.00");
		ArrayList<Category> categoriesCredit;

		categoryDBHelper = new CategoryIncomeDB(this);

		categoriesCredit = categoryDBHelper.getAllCategory();
		double expenseGrand = 0;
		for (Category category : categoriesCredit) {
			double expenseTotal = 0;

			ArrayList<Item> incomeItems;

			TableRow tr = new TableRow(this);
			tr.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
					LayoutParams.WRAP_CONTENT));
			Log.v("Check", category.getName());

			TextView addColumn = new TextView(this);
			itemDBHelper = new ItemIncomeDB(this);
			incomeItems = itemDBHelper.getItemByDateCategory(category.getCatid(),startDate,endDate);
			for (Item item : incomeItems) {
				expenseTotal = item.getItemAmount() + expenseTotal;
			}
			expenseGrand = expenseTotal + expenseGrand;
			addColumn.setText("Total " + category.getName() + ":");
			addColumn.setTextColor(Color.rgb(zero, FF, zero));
			addColumn.setTextSize(20);
			tr.addView(addColumn);
			addColumn = null;
			addColumn = new TextView(this);
			addColumn.setText(dollarFormat.format(expenseTotal));
			addColumn.setTextColor(Color.rgb(zero, zero, FF));
			addColumn.setTextSize(20);
			tr.addView(addColumn, 1);
			tl.addView(tr, new TableLayout.LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
			expenseTotal = 0;
		}
		TextView grandTotal = (TextView) findViewById(R.id.grandTotal);
		grandTotal.setText(dollarFormat.format(expenseGrand));
	}
}
