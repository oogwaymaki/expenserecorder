package com.trulycanadian.ExpenseRecorder.Activity;

import java.util.Date;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.trulycanadian.ExpenseRecorder.ExpenseRecorder;
import com.trulycanadian.ExpenseRecorder.R;
import com.trulycanadian.ExpenseRecorder.db.ReportDateDB;

public class SetTimePeriodActivity extends Activity{
    private TextView startDateField;
    private Button startDateButton;
    private int startYear;
    private int startMonth;
    private int startDay;
    private TextView endDateField;
    private Button endDateButton;
    private Button saveButton;
    private int endYear;
    private int endMonth;
    private int endDay;


    static final int START_DIALOG_ID = 2;
    static final int END_DIALOG_ID = 3;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.settimeperiod);

        // capture our View elements
        startDateField = (TextView) findViewById(R.id.startDateValue);
        startDateButton = (Button) findViewById(R.id.startDateButton);

        endDateField = (TextView) findViewById(R.id.endDateValue);
        endDateButton = (Button) findViewById(R.id.endDateButton);
        saveButton = (Button) findViewById(R.id.saveButton);
        
        // add a click listener to the button
        startDateButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(START_DIALOG_ID);
            }
        });
        endDateButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(END_DIALOG_ID);
            }
        });
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	ReportDateDB reportDateSaver = new ReportDateDB(SetTimePeriodActivity.this);
            	ExpenseRecorder app = (ExpenseRecorder) getApplication();
              Date startDate = new Date();
              Date endDate = new Date();
              startDate.setYear(startYear-1900);
              startDate.setDate(startDay);
              startDate.setMonth(startMonth);
              endDate.setMonth(endMonth);
              endDate.setDate(endDay);
              endDate.setYear(endYear-1900);
              app.setStartDate(startDate);
              app.setEndDate(endDate);
              reportDateSaver.updateStartDate(startDate);
              reportDateSaver.updateEndDate(endDate);
              Log.w("ENDDATE" ,endDate.toString());
              Log.w("STARTDATE",startDate.toString());
              finish();
            }
        });

    	ExpenseRecorder app = (ExpenseRecorder) getApplication();
		Date startDate = (Date) app.getStartDate();
		Date endDate = (Date) app.getEndDate();
    
   
        
        startYear = startDate.getYear() +1900;
        startMonth = startDate.getMonth();
        startDay = startDate.getDate();
        
        endYear = endDate.getYear() + 1900;
        endMonth = endDate.getMonth();
        endDay = endDate.getDate();

      
        // display the current date (this method is below)
        updateDisplay();

	    }
    // updates the date in the TextView
     private void updateDisplay() {
        startDateField.setText(
            new StringBuilder()
                    // Month is 0 based so add 1
                    .append(startMonth + 1).append("-")
                    .append(startDay).append("-")
                    .append(startYear).append(" "));
        endDateField.setText(
                new StringBuilder()
                        // Month is 0 based so add 1
                        .append(endMonth + 1).append("-")
                        .append(endDay).append("-")
                        .append(endYear).append(" "));
    }
     private DatePickerDialog.OnDateSetListener startDateSetListener =
         new DatePickerDialog.OnDateSetListener() {

             public void onDateSet(DatePicker view, int year, 
                                   int monthOfYear, int dayOfMonth) {
                startYear = year;
                startMonth = monthOfYear;
                startDay = dayOfMonth;
                updateDisplay();
             }
         };
         private DatePickerDialog.OnDateSetListener endDateSetListener =
             new DatePickerDialog.OnDateSetListener() {

                 public void onDateSet(DatePicker view, int year, 
                                       int monthOfYear, int dayOfMonth) {
                    endYear = year;
                    endMonth = monthOfYear;
                    endDay = dayOfMonth;
                    updateDisplay();
                 }
             };

         @Override
         protected Dialog onCreateDialog(int id) {
             switch (id) {
             case START_DIALOG_ID:
                 return new DatePickerDialog(this,
                             startDateSetListener,
                             startYear, startMonth, startDay);
             case END_DIALOG_ID:
                 return new DatePickerDialog(this,
                             endDateSetListener,
                             endYear, endMonth, endDay);
             }
             return null;
         }
}
