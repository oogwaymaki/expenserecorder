package com.trulycanadian.ExpenseRecorder.Activity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TableLayout;
import android.widget.TextView;

import com.trulycanadian.ExpenseRecorder.ExpenseRecorder;
import com.trulycanadian.ExpenseRecorder.Item;
import com.trulycanadian.ExpenseRecorder.R;
import com.trulycanadian.ExpenseRecorder.db.ItemExpenseDB;
import com.trulycanadian.ExpenseRecorder.db.ItemIncomeDB;

public class TotalSummaryActivity extends Activity {

	TableLayout tl;
	private static final int CHANGE_DATE = 0;
	private ItemIncomeDB incomeItemDB;
	private ItemExpenseDB expenseItemDB;
	private Date endDate;
	private Date startDate;
	int zero = 00;
	int FF = 255;

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.totalsummary);
		this.getWindow().setBackgroundDrawableResource(R.drawable.background);
        TextView startView = (TextView) this.findViewById(R.id.dateTimeTextFrom);
        TextView endView = (TextView) this.findViewById(R.id.dateTimeTextTo);
		ExpenseRecorder app = (ExpenseRecorder) getApplication();
		startDate = app.getStartDate();
		endDate = app.getEndDate();
        startView.setText(startDate.toLocaleString());
        endView.setText(endDate.toLocaleString());
        doTotalCalculations();
	}
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(0, CHANGE_DATE, 0, R.string.changeDate);
		return true;
	}

	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		case CHANGE_DATE:
			Intent timePeriod = new Intent(this, SetTimePeriodActivity.class);
			this.startActivity(timePeriod);
			return true;
		}
		return super.onMenuItemSelected(featureId, item);

	}
	public void doTotalCalculations()
	{
		DecimalFormat dollarFormat = new DecimalFormat(" ######0.00");
		ArrayList<Item> incomeItems;
		ArrayList<Item> expenseItems;
		double totalIncome = 0;
		double totalExpense =0;
		double difference = 0;
		double avgDailyExpense = 0;
		double avgWeeklyExpense = 0;
		long totalDays;
		Long totalWeeks;
		
		Long totalMilli = endDate.getTime() - startDate.getTime();
		totalDays = totalMilli / 1000 / 60 / 60 / 24;
		if (totalDays > 7)
		  totalWeeks = totalMilli / 1000 / 60 / 60 / 24 / 7;
		else
			totalWeeks=0L;
		Log.w("Number of days ",Long.toString(totalDays));
		
		incomeItemDB = new ItemIncomeDB(this);
		expenseItemDB = new ItemExpenseDB(this);
		incomeItems = incomeItemDB.getItemByDate(startDate, endDate);
		expenseItems = expenseItemDB.getItemByDate(startDate, endDate);
		for (Item item : incomeItems) 
			totalIncome = item.getItemAmount() + totalIncome;
		for (Item item : expenseItems) 
			totalExpense = item.getItemAmount() + totalExpense;
		
		avgDailyExpense = totalExpense / totalDays;
		avgWeeklyExpense = totalExpense / totalWeeks;
		
		difference = totalIncome - totalExpense;
		TextView incomeTotal = (TextView) findViewById(R.id.totalIncome);
		TextView expenseTotal = (TextView) findViewById(R.id.totalExpense);
		TextView differenceTotal = (TextView) findViewById(R.id.totalDifference);
		TextView weeklyHeading = (TextView) findViewById(R.id.averageWeekHeading);
		TextView weeklyExpense = (TextView) findViewById(R.id.averagePerWeek);
		TextView dailyExpense = (TextView) findViewById(R.id.averagePerDay);
		if (avgWeeklyExpense < 1 || totalWeeks < 1)
			weeklyHeading.setText("");
		else 
			weeklyExpense.setText(dollarFormat.format(avgWeeklyExpense));
		dailyExpense.setText(dollarFormat.format(avgDailyExpense));
		incomeTotal.setText(dollarFormat.format(totalIncome));
		expenseTotal.setText(dollarFormat.format(totalExpense));
		differenceTotal.setText(dollarFormat.format(difference));
	}
}