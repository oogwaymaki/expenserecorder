package com.trulycanadian.ExpenseRecorder;

public class Category {

	Long catid;
	String name;

	public Long getCatid() {
		return catid;
	}

	public void setCatid(Long catid) {
		this.catid = catid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
