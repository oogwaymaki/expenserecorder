package com.trulycanadian.ExpenseRecorder;

import java.util.Date;

import android.app.Application;

public class ExpenseRecorder extends Application {
	private Date startDate= null;
	private Date endDate = null;	
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public ExpenseRecorder()
	{
		super();
	}
}