package com.trulycanadian.ExpenseRecorder;

import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.trulycanadian.ExpenseRecorder.ListAdapter.IconMenuAdapter;
import com.trulycanadian.ExpenseRecorder.db.ReportDateDB;
import com.trulycanadian.ExpenseRecorder.factory.IntentFactory;

public class ExpenseRecorderActivity extends Activity {
	private static final int EXPORT_CSV = 7;
	private static final int ADD_CATEGORY = 10;
	private static final int DELETE_CATEGORY = 11;
	private static final int HELP = 12;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		IconMenuAdapter menuItems = new IconMenuAdapter(this);
		ExpenseRecorder app = (ExpenseRecorder) getApplication();
		Date startDate = new Date();
		Date endDate = new Date();
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.mainmenu);

		ListView listView = (ListView) this.findViewById(R.id.mainMenuList);
		final Calendar c = Calendar.getInstance();

		ReportDateDB reportDateLoader = new ReportDateDB(this);

		int startYear = c.get(Calendar.YEAR);
		int startMonth = (c.get(Calendar.MONTH) - 1);
		int startDay = 1;

		int endYear = c.get(Calendar.YEAR);
		int endMonth = c.get(Calendar.MONTH);
		int endDay = c.get(Calendar.DAY_OF_MONTH);

		startDate.setDate(startDay);
		startDate.setMonth(startMonth);
		startDate.setYear(startYear - 1900);

		endDate.setDate(endDay);
		endDate.setYear(endYear - 1900);
		endDate.setMonth(endMonth);

		Date tempDate = reportDateLoader.getStartDate();
		if (tempDate != null)
			app.setStartDate(tempDate);
		else
			app.setStartDate(startDate);
		tempDate = null;
		tempDate = reportDateLoader.getEndDate();
		if (tempDate != null)
			app.setEndDate(tempDate);
		else
			app.setEndDate(endDate);
		tempDate = null;

		this.getWindow().setBackgroundDrawableResource(R.drawable.background);
		menuItems.addItem(new IconMenu("Add Expense Item", getResources()
				.getDrawable(R.drawable.basket)));
		menuItems.addItem(new IconMenu("Add Income Item", getResources()
				.getDrawable(R.drawable.banknoteicon)));
		menuItems.addItem(new IconMenu("List Total Expenses", getResources()
				.getDrawable(R.drawable.calculatoricon)));
		menuItems.addItem(new IconMenu("List Total Incomes", getResources()
				.getDrawable(R.drawable.calculatoricon)));
		menuItems.addItem(new IconMenu("Total/Average Reports", getResources()
				.getDrawable(R.drawable.calculatoricon)));
		menuItems.addItem(new IconMenu("Select Time Period", getResources()
				.getDrawable(R.drawable.clockmoney)));
		menuItems.addItem(new IconMenu("Graphs", getResources().getDrawable(
				R.drawable.multicolorgraph)));
		;
		listView.setAdapter(menuItems);

		OnItemClickListener listener = new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {

				Intent menuItem = IntentFactory.createIntent(position,
						ExpenseRecorderActivity.this);
				ExpenseRecorderActivity.this.startActivity(menuItem);
			}
		};
		listView.setOnItemClickListener(listener);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(0, ADD_CATEGORY, 0, R.string.categoryAdd);
		menu.add(1, DELETE_CATEGORY, 0, R.string.categeoryDelete);
		menu.add(3, EXPORT_CSV, 0, R.string.exportCsv);
		menu.add(2, HELP, 0, R.string.Help);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		Intent menuItem = IntentFactory.createIntent(item.getItemId(), this);
		this.startActivity(menuItem);
		return super.onMenuItemSelected(featureId, item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);

	}

}
