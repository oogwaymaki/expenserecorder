package com.trulycanadian.ExpenseRecorder;


import android.graphics.drawable.Drawable;

public class IconMenu { 
    
     private String mText = ""; 
     private Drawable mIcon; 
     private boolean mSelectable = true; 

     public IconMenu(String text, Drawable bullet) { 
          mIcon = bullet; 
          mText = text; 
     } 
      
     public boolean isSelectable() { 
          return mSelectable; 
     } 
      
     public void setSelectable(boolean selectable) { 
          mSelectable = selectable; 
     } 
      
     public String getText() { 
          return mText; 
     } 
      
     public void setText(String text) { 
          mText = text; 
     } 
      
     public void setIcon(Drawable icon) { 
          mIcon = icon; 
     } 
      
     public Drawable getIcon() { 
          return mIcon; 
     } 
} 
