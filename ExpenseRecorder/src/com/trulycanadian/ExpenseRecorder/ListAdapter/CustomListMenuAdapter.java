package com.trulycanadian.ExpenseRecorder.ListAdapter;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.widget.SimpleAdapter;

public class CustomListMenuAdapter extends SimpleAdapter {

	List<? extends Map<String, ?>> data;

	public CustomListMenuAdapter(Context context,
			List<? extends Map<String, ?>> data, int resource, String[] from,
			int[] to) {
		super(context, data, resource, from, to);
		this.data = data;
	}

	public boolean areAllItemsSelectable() {
		return false;
	}

	public boolean isEnabled(int position) {
		if (data.get(position).get("item").toString().equals("Grand Total: ")) {
			return false;
		} else
			return true;
	}
}
