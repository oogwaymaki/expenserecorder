package com.trulycanadian.ExpenseRecorder.ListAdapter;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomSpinnerAdapter extends BaseAdapter {
    static class ViewHolder {
        TextView text;
        ImageView icon;
    }
	ArrayList<String> titles = new ArrayList<String>();
	ArrayList<Long> ids = new ArrayList<Long>();
	Context mContext;
	int mDropDownResource;
	int mFieldId = 0;
	LayoutInflater mInflater;

	public void addItem(String title, long id) {
		titles.add(title);
		ids.add(id);
	}

	public CustomSpinnerAdapter(Context context, int id) {
		mContext = context;
		mFieldId = id;
		mInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		return titles.size();
	}

	public Object getItem(int position) {

		return titles.get(position);
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return Long.valueOf((ids.get(position)));
	}

	public boolean areAllItemsSelectable() {
		return false;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		return createResource(position, convertView, parent,android.R.layout.simple_spinner_item);
	}

	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		return createResource(position, convertView, parent,android.R.layout.simple_spinner_dropdown_item);

	}

	public View createResource(int position, View convertView,
			ViewGroup parent, int resource) {
		View view;
		ViewHolder holder;
		view = convertView;
		if (view == null) {
			Log.w("Inside Inflate:","Creating Inflater");
			view = mInflater.inflate(resource, parent,false);
            holder = new ViewHolder();
            holder.text = (TextView) view.findViewById(android.R.id.text1);
            view.setTag(holder);
		} 
		else
			holder = (ViewHolder) view.getTag();
		holder.text.setText(getItem(position).toString());
		return view;
	}
}
