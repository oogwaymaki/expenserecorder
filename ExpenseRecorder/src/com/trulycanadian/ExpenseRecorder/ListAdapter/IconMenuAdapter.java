package com.trulycanadian.ExpenseRecorder.ListAdapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.trulycanadian.ExpenseRecorder.IconMenu;
import com.trulycanadian.ExpenseRecorder.view.IconMenuView;

public class IconMenuAdapter extends BaseAdapter {

	private List<IconMenu> mItems = new ArrayList<IconMenu>();
	private Context mContext;

	public boolean areAllItemsSelectable() {
		return false;
	}

	public void addItem(IconMenu mItem)
	{
		mItems.add(mItem);
	}
	public IconMenuAdapter(Context context) {
		mContext = context;
	}

	public int getCount() {
		return mItems.size();

	}

	public Object getItem(int position) {

		return mItems.get(position);
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		 return new IconMenuView(mContext, mItems.get(position)); 
	}

}
