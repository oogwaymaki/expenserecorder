package com.trulycanadian.ExpenseRecorder.db;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.trulycanadian.ExpenseRecorder.Category;
import com.trulycanadian.ExpenseRecorder.Item;

public class AdapterDB {
	public static final String KEY_ID = "_id";
	public static final String KEY_ITEM = "item";
	public static final String KEY_AMOUNT = "amount";
	public static final String KEY_CATEGORY = "category";
	public static final String KEY_CATFOREIGNID = "_catid";
	public static final String KEY_DATE = "dateInsert"; 
	public static final String KEY_STARTDATE ="dateStart";
	public static final String KEY_ENDDATE ="dateEnd";
	
	private static final String TAG = "moneyItem";

	protected static final String DATABASE_NAME = "accountLedger";
	protected static final String DATABASE_INCOMEITEMS = "incomeitems";
	protected static final String DATABASE_EXPENSEITEMS = "expenseitems";
	protected static final String DATABASE_CATEGORYEXPENSE = "categoriesexpense";
	protected static final String DATABASE_LASTDATE = "lastdate";
	private static final int DATABASE_VERSION = 700;

	protected static final String DATABASE_CATEGORYINCOME = "categoriesincome";
	protected final Context mCtx;
	private DatabaseHelper mDbHelper;
	protected SQLiteDatabase mDb;

	/**
	 * Database creation sql statement
	 */
	private static final String DATABASE_CREATE_EXPENSEITEMS = "create table "
			+ DATABASE_EXPENSEITEMS
			+ "(_id integer primary key autoincrement, "
			+ "item text not null, amount decimal not null,dateInsert date,"
			+ " _catid integer not null);";
	private static final String DATABASE_CREATE_INCOMEITEMS = "create table "
			+ DATABASE_INCOMEITEMS + "(_id integer primary key autoincrement, "
			+ "item text not null, amount decimal not null, dateInsert date,"
			+ " _catid integer not null);";
	private static final String DATABASE_CREATE_CATEGORYEXPENSE = "create table "
			+ DATABASE_CATEGORYEXPENSE
			+ "(_id integer primary key autoincrement, "
			+ "category text not null,maxAmount decimal);";
	private static final String DATABASE_CREATE_CATEGORYINCOME = "create table "
			+ DATABASE_CATEGORYINCOME
			+ "(_id integer primary key autoincrement, "
			+ "category text not null,maxAmount decimal);";
	private static final String DATABASE_CREATE_LASTDATE = "create table "
		+ DATABASE_LASTDATE 
		+ "(_id integer primary key autoincrement,dateStart date, dateEnd date);";
	private static final String DATABASE_ALTERINCOME =  "alter table "
		+ DATABASE_CATEGORYEXPENSE 
		+ " add maxAmount decimal";
	private static final String DATABASE_ALTEREXPENSE = "alter table "
		+ DATABASE_CATEGORYINCOME 
		+ " add maxAmount decimal";
	
	private static class DatabaseHelper extends SQLiteOpenHelper {

		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {

			db.execSQL(DATABASE_CREATE_CATEGORYEXPENSE);
			db.execSQL(DATABASE_CREATE_CATEGORYINCOME);
			db.execSQL(DATABASE_CREATE_EXPENSEITEMS);
			db.execSQL(DATABASE_CREATE_INCOMEITEMS);
			db.execSQL(DATABASE_CREATE_LASTDATE);
			createDates(db);
			insertExpenseCategories(db);
			insertDebitCategories(db);
		}

		public void insertExpenseCategories(SQLiteDatabase db) {
			ContentValues values = new ContentValues();
			values.put("category", "Restaurant");
			db.insertOrThrow(DATABASE_CATEGORYEXPENSE, null, values);
			values.put("category", "Date");
			db.insertOrThrow(DATABASE_CATEGORYEXPENSE, null, values);
			values.put("category", "Movie");
			db.insertOrThrow(DATABASE_CATEGORYEXPENSE, null, values);
			values.put("category", "Electronics");
			db.insertOrThrow(DATABASE_CATEGORYEXPENSE, null, values);
			values.put("category", "SuperMarket");
			db.insertOrThrow(DATABASE_CATEGORYEXPENSE, null, values);
			values.put("category", "Personal Hygene");
			db.insertOrThrow(DATABASE_CATEGORYEXPENSE, null, values);
			values.put("category", "Rent/Living Expenses");
			db.insertOrThrow(DATABASE_CATEGORYEXPENSE, null, values);
			values.put("category", "Donations");
			db.insertOrThrow(DATABASE_CATEGORYEXPENSE, null, values);
			values.put("category", "Loans/CreditCards");
			db.insertOrThrow(DATABASE_CATEGORYEXPENSE, null, values);
		}

	
		public void insertDebitCategories(SQLiteDatabase db) {
			ContentValues values = new ContentValues();
			values.put("category", "Government");
			db.insertOrThrow(DATABASE_CATEGORYINCOME, null, values);
			values.put("category", "Work Pay Check");
			db.insertOrThrow(DATABASE_CATEGORYINCOME, null, values);
			values.put("category", "Gift");
			db.insertOrThrow(DATABASE_CATEGORYINCOME, null, values);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w("VERSION OLD",Integer.toString(oldVersion));
			Log.w("NEW VERSION",Integer.toString(newVersion));
			
			if (oldVersion < 600)
			{
				db.execSQL(DATABASE_CREATE_LASTDATE);
				createDates(db);
				db.execSQL(DATABASE_ALTERINCOME);
				db.execSQL(DATABASE_ALTEREXPENSE);
			}
		}
		private void createDates(SQLiteDatabase db)
		{
			final Calendar c = Calendar.getInstance();
			Date startDate = new Date();
			Date endDate = new Date();
			
			int startYear = c.get(Calendar.YEAR);
			int startMonth = c.get(Calendar.MONTH) - 1;
			int startDay = 1;

			int endYear = c.get(Calendar.YEAR);
			int endMonth = c.get(Calendar.MONTH);
			int endDay = c.get(Calendar.DAY_OF_MONTH);
			startDate.setDate(startDay);
			startDate.setMonth(startMonth);
			startDate.setYear(startYear - 1900);

			endDate.setDate(endDay);
			endDate.setYear(endYear - 1900);
			endDate.setMonth(endMonth);
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			ContentValues args = new ContentValues();
			args.put(KEY_STARTDATE, dateFormat.format(startDate));
			args.put(KEY_ENDDATE, dateFormat.format(endDate));
			Log.w("startdate", KEY_STARTDATE);
			db.insertOrThrow(DATABASE_LASTDATE,null,args);
		}
	}

	
	public void insertEntriesInDataBase(ContentValues values, String table) {
		mDb.insert(table, null, values);
	}

	public AdapterDB(Context ctx) {
		this.mCtx = ctx;
	}

	public AdapterDB open() throws SQLException {

		mDbHelper = new DatabaseHelper(mCtx);
		mDb = mDbHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		mDbHelper.close();
	}

	public Cursor fetchAllCreditCategory() {
		return mDb.query(DATABASE_CATEGORYEXPENSE, new String[] { KEY_ID,
				KEY_CATEGORY }, null, null, null, null, null);
	}

	public ArrayList<String> selectAllCategoryDebit() {
		ArrayList<String> list = new ArrayList<String>();
		Cursor cursor = this.mDb.query(DATABASE_CATEGORYINCOME,
				new String[] { KEY_CATEGORY }, null, null, null, null, null);
		if (cursor.moveToFirst()) {
			do {
				list.add(cursor.getString(0));
			} while (cursor.moveToNext());
		}
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		return list;
	}

	public Cursor returnAllCategoriesExpense() {
		Cursor cursor = this.mDb.query(DATABASE_CATEGORYEXPENSE,
				new String[] { KEY_CATEGORY }, null, null, null, null, null);
		return cursor;
	}

	public Cursor returnAllCategoriesIncome() {
		Cursor cursor = this.mDb.query(DATABASE_CATEGORYINCOME,
				new String[] { KEY_CATEGORY }, null, null, null, null, null);
		return cursor;
	}

	
	public ArrayList<String> selectAllCategoryCredit() {
		ArrayList<String> list = new ArrayList<String>();
		Cursor cursor = this.mDb.query(DATABASE_CATEGORYEXPENSE,
				new String[] { KEY_CATEGORY }, null, null, null, null, null);
		if (cursor.moveToFirst()) {
			do {
				list.add(cursor.getString(0));
			} while (cursor.moveToNext());
		}
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		return list;
	}
	public void deleteRecord(String table, Long id)
	{
		mDb.delete(table, KEY_ID + "= ?", new String [] {id.toString()});
	}	
    public void updateItem(String database,Item item) {
        ContentValues args = new ContentValues();
        args.put(KEY_ITEM, item.getItemName());
        args.put(KEY_CATFOREIGNID, item.getCatId());
        args.put(KEY_AMOUNT,item.getItemAmount());
        args.put(KEY_DATE, item.getStringDate());
        mDb.update(database, args, KEY_ID + "=" + item.getId(), null);
    }
    public void updateDate(String dateField,Date date) {
        ContentValues args = new ContentValues();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        args.put(dateField, dateFormat.format(date));
        mDb.update(DATABASE_LASTDATE, args, null, null);
    }
    public void updateCategory(String database, Category category)
    {
        ContentValues args = new ContentValues();
        args.put(KEY_CATEGORY, category.getName());
        mDb.update(database, args, KEY_ID + "=" + category.getCatid(),null);
    }
}
