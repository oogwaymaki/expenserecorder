package com.trulycanadian.ExpenseRecorder.db;

import java.util.ArrayList;

import com.trulycanadian.ExpenseRecorder.Category;
import com.trulycanadian.ExpenseRecorder.Item;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class CategoryDB extends AdapterDB {
	String tableName;

	public CategoryDB(Context ctx) {
		super(ctx);
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public Category getCategory(String categoryName) {
		Category category = new Category();
		super.open();
		Cursor mCursor =

		mDb.query(true, tableName, new String[] { KEY_ID, KEY_CATEGORY },
				KEY_CATEGORY + "= ? ", new String[] { categoryName }, null,
				null, null, null);
		if ((mCursor != null) && (mCursor.getCount() != 0)) {
			mCursor.moveToFirst();
			category.setCatid(mCursor.getLong(0));
			category.setName(mCursor.getString(1));
		}
		mCursor.close();
		super.close();
		return category;

	}

	public ArrayList<Category> getAllCategory() {
		ArrayList<Category> categoryList = new ArrayList<Category>();
		super.open();
		Cursor cursor = this.mDb.query(tableName, new String[] { KEY_ID,
				KEY_CATEGORY }, null, null, null, null, null);
		if (cursor.moveToFirst()) {
			do {
				Category category = new Category();
				category.setCatid(cursor.getLong(0));
				category.setName(cursor.getString(1));
				categoryList.add(category);
			} while (cursor.moveToNext());
		}
		super.close();
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		return categoryList;
	}


	public Cursor getAllCategoryCursor() {
		Cursor cursor = this.mDb.query(tableName, new String[] { KEY_ID,
				KEY_CATEGORY }, null, null, null, null, null);
		return cursor;
	}
	public Category getCatName(Long catId) {
		Category category = new Category();
		super.open();
		Log.w("DB CHECK CATEGORY", catId.toString());
		Cursor mCursor =

		mDb.query(true, tableName, new String[] { KEY_ID, KEY_CATEGORY },
				KEY_ID + "=" + catId, null, null, null, null, null);

		if (mCursor != null) {
			mCursor.moveToFirst();
			category.setCatid(mCursor.getLong(0));
			category.setName(mCursor.getString(1));
		}
		super.close();
		return category;
	}

	public void addCategory(String name) {
		super.open();
		ContentValues values = new ContentValues();
		values.put("category", name);
		mDb.insertOrThrow(tableName, null, values);
		super.close();
	}

	public void editCategory(Category category) {
		super.open();
		super.updateCategory(tableName, category);
		super.close();
	}


	public boolean checkHasCategoryExists(String category) {
		boolean success = false;
		super.open();
		Log.w("DB EXISTS", category);
		Cursor mCursor =

		mDb.query(true, tableName, new String[] { KEY_ID, KEY_CATEGORY },
				KEY_CATEGORY + "= ?", new String[] { category }, null, null, null, null);
		if (mCursor.getCount() != 0)
			success = true;
		else
			success = false;
		super.close();
		return success;
	}
	public void deleteRecord(long id)
	{
		super.open();
		super.deleteRecord(tableName, id);
		super.close();
	}
}
