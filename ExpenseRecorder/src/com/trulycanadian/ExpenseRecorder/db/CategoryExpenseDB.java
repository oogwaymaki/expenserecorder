package com.trulycanadian.ExpenseRecorder.db;

import android.content.Context;

public class CategoryExpenseDB extends CategoryDB {

	public CategoryExpenseDB(Context ctx) {
		super(ctx);
		super.setTableName(DATABASE_CATEGORYEXPENSE);
	}
}
