package com.trulycanadian.ExpenseRecorder.db;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.trulycanadian.ExpenseRecorder.Category;
import com.trulycanadian.ExpenseRecorder.Item;

public class ItemDB extends AdapterDB {
	Context ctx;
	String tableName;
	public void setTableName(String tableName)
	{
		this.tableName = tableName;
	}
	public String getTableName() {
		return tableName;
	}
	public ItemDB(Context ctx)
	{
		super(ctx);
		this.ctx = ctx;
	}
	public void save(Item item) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		ContentValues values = new ContentValues();
		values.put("dateInsert", dateFormat.format(item.getDate()));
		values.put("amount", item.getItemAmount());
		values.put("item", item.getItemName());
		values.put("_catid", item.getCatId());
		super.open();
		super.insertEntriesInDataBase(values, tableName);
		super.close();
	}

	public ArrayList<Item> getItemByDateCategory(Long catid,
			Date startDate, Date endDate) {
		ArrayList<Item> items = new ArrayList<Item>();
		super.open();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Cursor cursor = mDb.query(false, tableName, new String[] {
				KEY_ID, KEY_ITEM, KEY_AMOUNT, KEY_CATFOREIGNID, KEY_DATE },
				KEY_CATFOREIGNID + "= ? AND " + KEY_DATE + " >= ? AND "
						+ KEY_DATE + " <=  ? ", new String[] {
						catid.toString(), dateFormat.format(startDate),
						dateFormat.format(endDate) }, null, null, "dateInsert Desc", null);
		if (cursor.moveToFirst()) {
			do {
				Item item = new Item();
				item.setId(cursor.getLong(0));
				item.setItemName(cursor.getString(1));
				item.setItemAmount(cursor.getDouble(2));
				item.setCatId(cursor.getLong(3));
				item.setDate(convertDate(cursor.getString(4)));
				items.add(item);
			} while (cursor.moveToNext());
		}
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		cursor.close();
		super.close();
		return items;
	}

	public void deleteItem(Long id)
	{
		super.open();
		deleteRecord(tableName,id);
		super.close();
		
	}
	public ArrayList<Item> getItemByDate(Date startDate, Date endDate) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		ArrayList<Item> items = new ArrayList<Item>();
		super.open();
		Cursor cursor = mDb.query(false, tableName, new String[] {
				KEY_ID, KEY_ITEM, KEY_AMOUNT, KEY_CATFOREIGNID, KEY_DATE },
				KEY_DATE + " >= ? AND " + KEY_DATE + " <=  ? ", new String[] {
						dateFormat.format(startDate),
						dateFormat.format(endDate) }, null, null,"dateInsert Desc", null);
		if (cursor.moveToFirst()) {
			do {
				Item item = new Item();
				item.setId(cursor.getLong(0));
				item.setItemName(cursor.getString(1));
				item.setItemAmount(cursor.getDouble(2));
				item.setCatId(cursor.getLong(3));
				
				item.setDate(convertDate(cursor.getString(4)));
				items.add(item);
			} while (cursor.moveToNext());
		}
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		cursor.close();
		super.close();
		return items;
	}
	public Date convertDate(String convertDate)
	{
		Date date = new Date();
		String[] dateItems;
		dateItems = convertDate.split("-");	
		date.setYear(Integer.parseInt(dateItems[0])-1900);
		date.setMonth(Integer.parseInt(dateItems[1])-1);
		date.setDate(Integer.parseInt(dateItems[2]));
		return date;
	}

	public String checkCategoryHasItem(Long id) {
		String date = null;
		super.open();
		Cursor cursor = mDb.query(false, tableName, new String[] { "MIN("
				+ KEY_DATE + ")" }, KEY_CATFOREIGNID + "= " + id, null,
				null, null, null, null);
		if (cursor.getCount() != 0)
			if (cursor.moveToFirst()) {
				do {
					date = cursor.getString(0);
				} while (cursor.moveToNext());
			};
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		cursor.close();
		super.close();
		return date;
	}
	public void updateItem(Item item)
	{
		super.open();
		super.updateItem(tableName, item);
		super.close();
	}
}
