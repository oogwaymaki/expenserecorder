package com.trulycanadian.ExpenseRecorder.db;

import com.trulycanadian.ExpenseRecorder.Item;

import android.content.Context;
import android.database.Cursor;



public class ItemExpenseDB extends ItemDB {

	Context ctx;
	String tableName;
	public ItemExpenseDB(Context ctx) {
		super(ctx);
		this.ctx = ctx;
		super.setTableName(DATABASE_EXPENSEITEMS);
	}
	public Item getItem (Long id)
	{
		Item item = new Item();
		super.open();
		Cursor cursor = mDb.query(false, super.getTableName(), new String[] {
				KEY_ID, KEY_ITEM, KEY_AMOUNT, KEY_CATFOREIGNID , KEY_DATE},
				KEY_ID +" =?", new String[] {
					id.toString()}, null, null,null, null);
		if (cursor.moveToFirst()) {
		
				
				item.setId(cursor.getLong(0));
				item.setItemName(cursor.getString(1));
				item.setItemAmount(cursor.getDouble(2));
				item.setCatId(cursor.getLong(3));
				item.setDate(convertDate(cursor.getString(4)));
		}
		CategoryExpenseDB category = new CategoryExpenseDB(ctx);
		item.setCategory(category.getCatName(item.getCatId()).getName());
		
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		cursor.close();
		super.close();
		return item;
	}

}
