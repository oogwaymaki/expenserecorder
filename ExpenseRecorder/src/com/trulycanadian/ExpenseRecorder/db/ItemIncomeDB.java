package com.trulycanadian.ExpenseRecorder.db;

import android.content.Context;
import android.database.Cursor;

import com.trulycanadian.ExpenseRecorder.Item;

public class ItemIncomeDB extends ItemDB {
	Context ctx;

	public ItemIncomeDB(Context ctx) {
		super(ctx);
		this.ctx = ctx;
		super.setTableName(DATABASE_INCOMEITEMS);
	}
	public Item getItem (Long id)
	{
		Item item = new Item();
		super.open();
		Cursor cursor = mDb.query(false, super.getTableName(), new String[] {
				KEY_ID, KEY_ITEM, KEY_AMOUNT, KEY_CATFOREIGNID , KEY_DATE},
				KEY_ID +" =?", new String[] {
					id.toString()}, null, null,null, null);
		if (cursor.moveToFirst()) {
		
				
				item.setId(cursor.getLong(0));
				item.setItemName(cursor.getString(1));
				item.setItemAmount(cursor.getDouble(2));
				item.setCatId(cursor.getLong(3));
				item.setDate(convertDate(cursor.getString(4)));
		}
		CategoryIncomeDB category = new CategoryIncomeDB(ctx);
		item.setCategory(category.getCatName(item.getCatId()).getName());
		
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		cursor.close();
		super.close();
		return item;
	}
}