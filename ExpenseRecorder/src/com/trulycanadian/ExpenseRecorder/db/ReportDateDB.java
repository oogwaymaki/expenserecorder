package com.trulycanadian.ExpenseRecorder.db;

import java.util.Date;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class ReportDateDB extends AdapterDB {
	public ReportDateDB(Context ctx) {
		super(ctx);
	}

	public Date getStartDate() {

		String date = null;
		super.open();
		Cursor cursor = mDb.query(false, DATABASE_LASTDATE,
				new String[] { KEY_STARTDATE }, null, null, null, null, null, null);
		if (cursor.getCount() != 0)
			if (cursor.moveToFirst()) {
				do {
					date = cursor.getString(0);
				} while (cursor.moveToNext());
			}
		;
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		cursor.close();
		super.close();
		if (date != null)
			return convertDate(date);
		return null;
	}

	public Date getEndDate() {

		String date = null;
		super.open();
		Cursor cursor = mDb.query(false, DATABASE_LASTDATE,
				new String[]  {KEY_ENDDATE }, null, null, null, null, null, null);
		if (cursor.getCount() != 0)
			if (cursor.moveToFirst()) {
				do {
					date = cursor.getString(0);
				} while (cursor.moveToNext());
			};
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		cursor.close();
		super.close();
		if (date != null)
			return convertDate(date);
		return null;
	}

	public Date convertDate(String convertDate) {
		Date date = new Date();
		String[] dateItems;
		dateItems = convertDate.split("-");
		date.setYear(Integer.parseInt(dateItems[0]) - 1900);
		date.setMonth(Integer.parseInt(dateItems[1]) - 1);
		date.setDate(Integer.parseInt(dateItems[2]));
		return date;
	}
	public void updateEndDate(Date date)
	{
		super.open();
		Log.w("ENDDATE",date.toLocaleString());
		super.updateDate(KEY_ENDDATE,date);
		super.close();
	}
	public void updateStartDate(Date date)
	{
		super.open();
		super.updateDate(KEY_STARTDATE,date);
		super.close();
	}
}
