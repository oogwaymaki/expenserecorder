package com.trulycanadian.ExpenseRecorder.factory;

import com.trulycanadian.ExpenseRecorder.ExpenseRecorderActivity;
import com.trulycanadian.ExpenseRecorder.Activity.AddCategoryActivity;
import com.trulycanadian.ExpenseRecorder.Activity.DeleteCategoryActivity;
import com.trulycanadian.ExpenseRecorder.Activity.DisplayChart;
import com.trulycanadian.ExpenseRecorder.Activity.ExpenseCategoryListSummaryActivity;
import com.trulycanadian.ExpenseRecorder.Activity.ExpenseItemActivity;
import com.trulycanadian.ExpenseRecorder.Activity.ExportCsvActivity;
import com.trulycanadian.ExpenseRecorder.Activity.HelpActivity;
import com.trulycanadian.ExpenseRecorder.Activity.IncomeCategoryListSummaryActivity;
import com.trulycanadian.ExpenseRecorder.Activity.IncomeItemActivity;
import com.trulycanadian.ExpenseRecorder.Activity.SetTimePeriodActivity;
import com.trulycanadian.ExpenseRecorder.Activity.TotalSummaryActivity;

import android.content.Intent;

public class IntentFactory {

	private static final int ADD_EXPENSE = 0;
	private static final int ADD_INCOME = 1;
	private static final int LIST_EXPENSES = 2;
	private static final int LIST_INCOME = 3;
	private static final int LIST_SUMMARY = 4;
	private static final int SET_TAXES = 5;
	private static final int RUN_GRAPH = 6;
	private static final int EXPORT_CSV = 7;
	private static final int ADD_CATEGORY = 10;
	private static final int DELETE_CATEGORY = 11;
	private static final int HELP = 12;
	
	
	static public Intent createIntent(int position, ExpenseRecorderActivity activity) {

		switch (position) {
		case LIST_EXPENSES:
			Intent listExpenses = new Intent(activity,
					ExpenseCategoryListSummaryActivity.class);
			return listExpenses;
		case ADD_EXPENSE:
			Intent addCredit = new Intent(activity, ExpenseItemActivity.class);
			return addCredit;
		case ADD_INCOME:
			Intent addDebit = new Intent(activity, IncomeItemActivity.class);
			return addDebit;
		case LIST_INCOME:
			Intent debitExpenses = new Intent(activity,
					IncomeCategoryListSummaryActivity.class);
			return debitExpenses;
		case LIST_SUMMARY:
			Intent totalSummary = new Intent(activity,
					TotalSummaryActivity.class);
			return totalSummary;

		case SET_TAXES:
			Intent setTaxesPeriod = new Intent(activity,
					SetTimePeriodActivity.class);
			return setTaxesPeriod;
		case RUN_GRAPH:
			Intent setGraph = new Intent(activity, DisplayChart.class);
			return setGraph;
			
		case ADD_CATEGORY:
			Intent addCategory = new Intent(activity, AddCategoryActivity.class);
			return addCategory;
		case DELETE_CATEGORY:
			Intent deleteCategory = new Intent(activity,
					DeleteCategoryActivity.class);
			return deleteCategory;
		case EXPORT_CSV:
			Intent exportCSV = new Intent(activity,ExportCsvActivity.class);
			return exportCSV;
		case HELP:
			Intent helpActivity = new Intent(activity, HelpActivity.class);
			return helpActivity;
			
		default:
			Intent intent = null;
			return intent;
		}
	}
}
