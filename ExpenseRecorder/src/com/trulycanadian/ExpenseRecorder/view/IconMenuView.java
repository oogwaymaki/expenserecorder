package com.trulycanadian.ExpenseRecorder.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.trulycanadian.ExpenseRecorder.IconMenu;

public class IconMenuView extends LinearLayout {

	int zero = 00;
	int FF = 255;
	private TextView mText;
	private ImageView mIcon;

	public IconMenuView(Context context, IconMenu iconMenu) {
		super(context);

		/*
		 * 
		 * First Icon and the Text to the right (horizontal), not above and
		 * below (vertical)
		 */
		this.setOrientation(HORIZONTAL);

		mIcon = new ImageView(context);
		mIcon.setImageDrawable(iconMenu.getIcon());
		// left, top, right, bottom
		mIcon.setPadding(0, 2, 5, 0); // 5px to the right

		/*
		 * At first, add the Icon to ourself (! we are extending LinearLayout)
		 */
		addView(mIcon, new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));

		mText = new TextView(context);
		mText.setText(iconMenu.getText());
		mText.setTextColor(Color.rgb(zero, FF,zero ));
		mText.setTextSize(20);
		/* Now the text (after the icon) */
		addView(mText, new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));
	}

	public void setText(String words) {
		mText.setText(words);
	}

	public void setIcon(Drawable bullet) {
		mIcon.setImageDrawable(bullet);
	}
}
